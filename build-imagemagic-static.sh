#!/bin/bash

set -e

brew install pkg-config
brew install glib
brew install lcms2

IMBUILD=/tmp/imbuild/
mkdir -p $IMBUILD/bin

# get IM (Version 7 at the moment)
mkdir im_download && cd im_download
curl -O http://www.imagemagick.org/download/ImageMagick.tar.gz
for file in *.gz ; do tar zxf $file; done
rm *.tar.gz
cd ImageM*

# get delegates
# jpeg
echo ">>>>>>>>>>>>>> JPEG"
curl -O http://www.imagemagick.org/download/delegates/jpegsrc.v9b.tar.gz
tar zxf jpeg*.gz && rm jpeg*.gz && mv jpeg* jpeg && cd jpeg
./configure --disable-shared --disable-dependency-tracking
make
cd ..

# ufraw
echo ">>>>>>>>>>>>>> UFRAW"
curl -L https://kent.dl.sourceforge.net/project/ufraw/ufraw/ufraw-0.22/ufraw-0.22.tar.gz > ufraw-0.22.tar.gz
tar zxf ufraw*.gz && rm ufraw*.gz && cd ufraw
./configure --disable-shared --disable-dependency-tracking
make
cd ..

# libraw
# echo ">>>>>>>>>>>>>> LibRaw"
# curl -O https://imagemagick.org/download/delegates/LibRaw-0.18.5.tar.gz
# tar zxf LibRaw*.gz && rm LibRaw*.gz && mv LibRaw* LibRaw && cd LibRaw
# ./configure --disable-shared --disable-dependency-tracking
# make
# cd ..

# png
echo ">>>>>>>>>>>>>> PNG"
curl -O http://www.imagemagick.org/download/delegates/libpng-1.6.31.tar.gz
tar zxf libpng*.gz && rm libpng*.gz && mv libpng* png && cd png
./configure --disable-shared --disable-dependency-tracking
make
cd ..

# freetype
echo ">>>>>>>>>>>>>> FREETYPE"
curl -O http://www.imagemagick.org/download/delegates/freetype-2.8.1.tar.gz
tar zxf freetype*.gz && rm freetype*.gz && mv freetype* freetype && cd freetype
./configure --disable-shared --disable-dependency-tracking
make
cd ..

# tiff
echo ">>>>>>>>>>>>>> TIFF"
curl -O http://www.imagemagick.org/download/delegates/tiff-4.0.8.tar.gz
tar zxf tiff*.gz && rm tiff*.gz && mv tiff* tiff && cd tiff
./configure --disable-shared --disable-dependency-tracking
make
cd ..

#build imagemagick (YMMV)
echo ">>>>>>>>>>>>>> IMAGEMAGICK"
./configure --disable-shared \
    --disable-dependency-tracking \
    --disable-shared \
    --enable-osx-universal-build \
    --enable-delegate-build \
    --disable-installed \
    --without-frozenpaths \
    --prefix=$IMBUILD \
    --with-openexr=no \
    --disable-docs \
    --without-lcms \
    --without-x \
    --without-webp \
    --with-jpeg \
    --without-pango \
    --enable-hdri=no \
    --without-gvc \
    --with-raw

make install

ls $IMBUILD/bin
